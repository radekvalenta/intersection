'use strict';

class TrafficLight {
  constructor(yellowTime=30000) {
    this.red = true;
    this.yellow = false;
    this.green = false;
    this.timer = yellowTime;
  }

  changeToRed() {
    // yellow is on for 30s before switch to green
    let p = new Promise((resolve, reject) => {
      this.red = false;
      this.green = false;
      this.yellow = true;
      setTimeout(() => {
        this.yellow = false;
        this.red = true;
        resolve();
      }, this.timer);
    });
    return p;
  }

  changeToGreen() {
    this.red = false;
    this.green = true;
  }

  currentState() {
    if(this.red) {
      return 'red';
    }
    if(this.yellow) {
      return 'yellow';
    }
    if(this.green) {
      return 'green';
    }
  }
}

module.exports = TrafficLight;
