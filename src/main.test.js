let chai = require('chai');
let TrafficLight = require('./trafficLight.js');

chai.should();

describe('init and change traffic light to green', function() {
  let trafficLight;

  beforeEach(() => {
    trafficLight = new TrafficLight();
  });

  it('should have red on and yellow and green off', () => {
    trafficLight.red.should.be.true;
    trafficLight.yellow.should.be.false;
    trafficLight.green.should.be.false;
  });

  it('should change light to green', () => {
    trafficLight.changeToGreen();
    trafficLight.red.should.be.false;
    trafficLight.yellow.should.be.false;
    trafficLight.green.should.be.true;
  });

});

describe('change traffic light to red', function() {
  let trafficLight;
  let timer = 30000; // yellow should be on for 30s before change to green

  beforeEach(() => {
    trafficLight = new TrafficLight();
  });
  // allows light to update before timeout
  this.timeout(timer + 10);

  it('should change light to yellow first', () => {
    trafficLight.changeToRed();
    trafficLight.red.should.be.false;
    trafficLight.yellow.should.be.true;
    trafficLight.green.should.be.false;
  });

  it('yellow light should be on for 30s', () => {
    trafficLight.changeToRed();
    setTimeout(function() {
      trafficLight.red.should.be.false;
      trafficLight.yellow.should.be.true;
      trafficLight.green.should.be.false;
    }, timer - 10);
  });

  it('should change light to red after 30s', (done) => {
    trafficLight.changeToRed();
    setTimeout(function() {
      try {
        done();
      } catch(e) {
        done(e);
      }
      check(done, function() {
        trafficLight.red.should.be.true;
        trafficLight.yellow.should.be.false;
        trafficLight.green.should.be.false;
      });
    }, timer);
  });

});
