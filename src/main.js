'use strict';
let TrafficLight = require('./trafficLight.js');

let options = {
  lightTime: 300000,
  yellowTime: 30000,
  timeoutIn: 1800000
};

let clock = {
  min: 0,
  sec: 0
};

let trafficLightsNS = new TrafficLight(options.yellowTime);
let trafficLightsEW = new TrafficLight(options.yellowTime);

// creating bacic clock here, used for presentation only
function runClock() {
  setInterval(() => {
    clock.sec++;
    if(clock.sec === 60) {
      clock.min++;
      clock.sec = 0;
    }
  }, 1000);
}

// update HTML table in index.html
function displayState() {
  let tBody = document.getElementById('app');
  let innerHtml = `
    <td>${clock.min}m ${clock.sec}s</td>
    <td class="${trafficLightsNS.currentState()}"></td>
    <td class="${trafficLightsEW.currentState()}"></td>`;
  let newRow = document.createElement('tr');

  newRow.innerHTML = innerHtml;
  tBody.appendChild(newRow);
}

function intersectionSimulation(timeoutIn, lightTime) {
  let lightChangeNS, lightChangeEW;

  function stopCycle() {
    setTimeout(() => {
      clearTimeout(lightChangeNS, lightChangeEW);
    }, timeoutIn);
  }

  function intersectionNS() {
    trafficLightsEW.changeToRed().then(() => {
      trafficLightsNS.changeToGreen();
      displayState();
      lightChangeNS = setTimeout(() => {
        intersectionEW();
      }, lightTime);
    });
    // exit after 30m
    stopCycle();
  }

  function intersectionEW() {
    trafficLightsNS.changeToRed().then(() => {
      trafficLightsEW.changeToGreen();
      displayState();
      lightChangeEW = setTimeout(() => {
        intersectionNS();
      }, lightTime);
    });
    // exit after 30m
    stopCycle();
  }

  // start clock
  runClock();
  // first change one set on, to avoid wait for cycle to finish
  trafficLightsEW.changeToGreen();
  displayState();

  // than start running regular cycle
  setTimeout(() => {
    // light get changed every 5 minutes
    intersectionNS();
  }, lightTime);

}

// start simulation
intersectionSimulation(options.timeoutIn, options.lightTime);
